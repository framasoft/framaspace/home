title: Conditions Spécifiques d’Utilisation du service @:txt.space
md: |- # ⚠️ identation 2 espaces ⚠️
  ## Préambule

  Framasoft propose un service, sous la forme « d’espaces @:txt.space », correspondant à la **mise à disposition d’espaces collaboratifs** de partage et d’échange de fichiers et de données.

  En utilisant le service @:txt.space, vous acceptez d’être lié·e par les  conditions spécifiques suivantes.

  Ces conditions sont complémentaires et cumulatives aux Conditions Générales
  d’Utilisation des sites et services proposés par l’association Framasoft (ci-après « Conditions Générales Framasoft »), disponibles à l’adresse [https://framasoft.org/fr/cgu/](https://framasoft.org/fr/cgu/)

  Framasoft se réserve le droit de mettre à jour et modifier ces Conditions Spécifiques d’Utilisation du service @:txt.space de temps à autre (cf. « Partie III »).

  ## Version courte

  (TL;DR comme on dit chez nous 😉)

  Pour en faciliter la lecture, sans le blabla juridique, nous vous en proposons ci-dessous une version compréhensible par un être humain normal.

  ### Concernant les limitations

  * Les espaces sont basés sur le **logiciel libre Nextcloud**
  * Les espaces sont **réservés à des organisations, sur critères d’éligibilité**
  * Le service est en **bêta test**
      * La peinture est fraîche
      * ça peut planter
      * La quantité d’espaces augmentera petit à petit, sur plusieurs années, selon un rythme décidé par Framasoft
  * Limitations principales
      * Un espace = **50 comptes utilisateurs** maximum, **40 Go d’espace global** maximum, **500 Mo par fichier** maximum
      * Il est possible de choisir entre les suites bureautique **OnlyOffice** ou Collabora Online (**LibreOffice**), mais le nombre d’**utilisateurs concurrents pour éditer des documents** au sein d’un espace est limité à **20**.
      * La **personnalisation de l’espace est limitée** aux options fournies dans l’interface Nextcloud/@:txt.space
      * En cas d’inactivité de connexion de longue durée (3 mois), l’espace sera désactivé. Il sera supprimé au bout d’un mois et demi supplémentaire si aucune action attendue n’est constatée
      * Le **support est communautaire**, donc pas d’obligation de réponse
  * Framasoft finance et héberge ce service, et se réserve le **droit de toute action « autoritaire »** dessus (mise à jour, suppression de comptes, etc).
  * En dehors de ces limitations, l’acceptation des conditions spécifiques d’utilisation des services @:txt.space entraîne aussi celles de nos [conditions générales d’utilisation](https://framasoft.org/fr/cgu/).

  ### Concernant la vie privée et la gestion des données personnelles

  * **Framasoft est hébergeur technique** des machines physiques opérant le service @:txt.space, ainsi que prestataire technique du service @:(txt.space).
      * En conséquence, **Framasoft n’est « que » le sous-traitant** d’hébergement des données à caractères personnelles qui pourraient être hébergées sur les espaces @:(txt.space).
      * En conséquence, **Framasoft ne peut être reconnu comme responsable de traitement de ces données**. D’ailleurs **Framasoft s’engage à ne pas regarder/manipuler/exploiter vos données**, sauf pour motif de gestion technique du service, ou en cas de défaillance du Responsable de Traitement.
  * **Le Responsable de Traitement est la personne qui a souscrit au service @:txt.space** (c’est à dire l’administrateur du compte Nextcloud/@:txt.space), ou la personne mandatée publiquement par ce dernier
      * Le Responsable de Traitement devra s’assurer que **l’ensemble des utilisateurs de l’espace @:txt.space dont il a la responsabilité sont correctement informés** de leurs droits et devoirs concernant la gestion des données à caractère personnel.
      * Il est rappelé que l’hébergeur des machines physiques est la société européenne, de droit Allemand, [Hetzner](https://hetzner.com), et que cette dernière n’a pas de certification pour l’hébergement des données de santé.
      * Il lui incombe de faire respecter **l’exercice des droits** (accès, modification, suppression, etc) des utilisateurs dont il a la responsabilité.
      * **Les demandes** d’exercices de ces droits **doivent se faire auprès du Responsable de Traitement**.
          * Si ce dernier ne réagit pas, l’utilisateur pourra s’adresser à Framasoft.
  * La liste des données collectées, et l’usage qui en est fait, peut être consulté sur la page [Vie privée et RGPD](https://www.frama.space/abc/fr/rgpd/)

  ## Versions complètes

  ### Partie I - LIMITATIONS LIÉES À L’USAGE DU SERVICE @:txt.space

  Ce document vise à lister les principales limitations à l’usage du service @:(txt.space).

  **Si ces limitations ne vous conviennent pas (c’est OK pour nous), nous vous
  invitons alors à ne pas créer d’espace @:txt.space, à ne pas utiliser notre service,
  et à prendre contact avec d’autres fournisseurs de service Nextcloud.**

  #### Service en beta-test

  Le service est actuellement en phase de en beta-test

  Cela signifie que nous accueillerons dans une première phase 200 à 300 structures
  avant d’ouvrir plus largement la plateforme en 2023 et les années suivantes.

  Pendant cette phase travaillerons de concert avec ces structures améliorer le service.
  L’ouverture d’un compte @:txt.space est donc soumise au fait que les utilisateurs
  du service participent activement à l’amélioration du service, notamment
  par le biais de retours d’expériences partagés par le biais de questionnaires.

  Il est à noter que le service @:txt.space, comme tout service informatique,
  peut rencontrer des dysfonctionnements, rendant par exemple inaccessibles
  vos données et fichiers pendant des périodes indéterminées.

  #### Service basé sur Nextcloud

  Le service @:txt.space consiste, dans sa phase de bêta-test, essentiellement
  en la mise à disposition d’une « instance » du logiciel libre de collaboration
  Nextcloud <https://nextcloud.com>.

  En conséquence, les limitations (techniques, ergonomiques, etc) de Nextcloud
  se retrouvent *de facto* dans l’usage de @:(txt.space).

  Nextcloud est un logiciel édité par la société allemande Nextcloud GmbH.
  Mais Nextcloud est surtout un logiciel libre, son code source est donc ouvert,
  accessible et modifiable.

  En conséquence, les frustrations structurelles liées à l’usage de @:txt.space
  sont probablement à adresser à l’entreprise Nextcloud Gmbh et à la communauté
  Nextcloud <https://help.nextcloud.com> et non directement à Framasoft.

  #### Pas d’accès SSH/FTP/etc

  @:txt.space ne propose pas, pour différentes raisons, d’accès SSH ou (s)FTP à
  vos fichiers, votre base de données, ou les fichiers techniques (configuration,
  PHP, CSS, etc) de votre instance Nextcloud.
  Les échanges de données se font nécessairement au travers de la solution Nextcloud.

  #### Limitation de la typologie des utilisateurs

  Pour le moment, l’inscription au service @:txt.space est réservée à certains
  types d’organisations.

  cf FAQ « [@:txt.space, c’est pour qui ?](https://www.frama.space/abc/fr/faq#q-whom) »

  Les personnes souhaitant créer un espace ne correspondant pas aux critères
  d’éligibilité définis par Framasoft se verront refuser la création dudit espace.

  #### Limitation du nombre d’espaces

  Framasoft est une petite association française.
  Elle ne dispose pas des moyens nécessaires au déploiement illimité d’espaces
  @:txt.space (c’est à dire d’instances Nextcloud).

  En conséquence, la validation des créations d’espaces est faite de façon
  discrétionnaire par Framasoft, au rythme qui lui semble le plus adapté.

  Il n’est donc pas possible d’imposer à Framasoft la requête de création
  d’un espace @:(txt.space).

  #### Limitation du nombre de comptes

  Afin de pouvoir monter le service en puissance, de gérer sa mise à l’échelle
  technique (« scalabilité »), de s’assurer de sa performance, et de ne pas léser
  des organisations qui fourniraient un service équivalent en prestation payante,
  Framasoft a fait le choix de limiter le nombre de compte à : 50 comptes
  utilisateurs maximum par espace @:(txt.space).

  #### Limitation de l’espace disque utilisable

  Afin de pouvoir monter le service en puissance, de gérer sa mise à l’échelle
  technique (« scalabilité »), de s’assurer de sa performance, et de ne pas léser
  des organisations qui fourniraient un service équivalent en prestation payante,
  Framasoft a fait le choix de limiter l’espace disque à : 40 Go par espace
  @:txt.space maximum (au total, quelque soit l’usage individuel de chaque utilisateur).

  #### Limitation de poids de fichier

  Afin de pouvoir monter le service en puissance, de gérer sa mise à l’échelle
  technique (« scalabilité »), de s’assurer de sa performance, et de ne pas léser
  des organisations qui fourniraient un service équivalent en prestation payante,
  Framasoft a fait le choix de limiter le poids maximum à : 500 Mo par fichier.

  #### OnlyOffice ou Collabora au choix

  @:txt.space vous offre la possibilité de choisir entre les suites bureautiques
  collaborative [OnlyOffice](https://www.onlyoffice.com/fr/) ou [Collabora Online](https://www.collaboraoffice.com/) (LibreOffice Online).

  Ce choix est défini par l’administrateur de l’espace @:txt.space (qui peut
  l’ajuster à sa convenance via les paramètres de son espace) et valable pour
  l’ensemble des utilisateurs de son espace (il ne s’agit pas d’un choix
  individuel par utilisateur).

  Qu’il s’agisse de l’une ou l’autre de ces suites bureautique, il est à noter
  que le nombre d’utilisateurs simultanés concurrents est limité à : 20.

  Cf FAQ pour plus de détails.

  #### Configuration et personnalisation de l’espace

  La configuration et la personnalisation de votre espace @:txt.space sont limitées,
  notamment par le fait que vous n’avez pas accès aux fichiers de
  l’instance Nextcloud (`config.php`, fichier CSS, etc.).

  Par ailleurs, la liste d’applications Nextcloud que vous pouvez mettre en
  place est aussi limitée à une liste d’applications validées par Framasoft.
  Cette liste pourra évoluer dans le temps.

  #### Support communautaire

  L’association Framasoft s’emploiera, chaque fois qu’elle le pourra,
  à répondre aux requêtes des utilisateurs de @:(txt.space).

  Cependant, il n’est pas raisonnable de penser qu’une petite association aux
  moyens limités comme Framasoft puisse répondre individuellement aux (potentiels)
  dizaines ou centaines de milliers d’utilisateurs du service.

  C’est pourquoi l’essentiel du support concernant @:txt.space s’effectue publiquement,
  via [le forum dédié au service](https://forum.frama.space). Les échanges, questions et réponses reposant
  sur le bon vouloir de la communauté du service, Framasoft ne prend aucun
  engagement à apporter des réponses aux requêtes des utilisateurs.

  Les questions requérant des informations personnelles ou une intervention
  technique spécifique de Framasoft peuvent être posées via une interface spécifique.

  #### Limitation de période d’inactivité

   @:txt.space est un service dont le nombre de places est limité. Afin d’éviter de voir la plateforme occupée par des espaces ne servant que d’espace de « tests » ou servant uniquement à fournir de la mise à disposition publique de fichiers ou de la synchronisation mobile d’agendas (il existe d’autres services pour cela), Framasoft définit des périodes d’inactivité aux termes desquelles les espaces pourront être automatiquement supprimés.

   Ainsi :
  - Si nous ne constatons aucune activité de connexion (identification d’un membre sur votre espace @:txt.space) pendant 60 jours, nous vous enverrons un mail d’alerte.
    - 15 jours plus tard, si aucune activité de connexion n’est constatée, vous recevrez un second mail d’alerte.
    - 15 jours plus tard, si aucune activité de connexion n’est constatée, votre espace sera désactivé. Vous recevrez un email vous permettant de le réactiver immédiatement. L’intégralité de votre espace (fichiers, liens, calendriers, etc) renverront alors vers une page indiquant que l’espace a été désactivé et indiquant la procédure à suivre pour le réactiver immédiatement.
    - 15 jours plus tard, si l’administrateur de l’espace n’a pas demandé la réactivation de l’espace, il recevra un email l’informant de la prochaine suppression de l’espace.
    - 30 jours plus tard, si l’administrateur de l’espace n’a pas demandé la réactivation de l’espace, celui-ci et toutes les données associées seront définitivement supprimés.

  Cette suppression (sur 4,5 mois), nous permet de nous assurer que la plateforme Framaspace n’est pas utilisée qu’à des fins d’archivage ou de test, mais bien par de « vraies » associations qui en ont l’utilité. Et cela permet aussi de libérer une place pour une autre association.

  #### Gestion discrétionnaire du service

  @:txt.space est un service mis à disposition et géré par l’association Framasoft.
  Le service est financé à 100% par Framasoft, sur la base de dons perçus par l’association.

  En conséquence, Framasoft se réserve le droit de toute action technique
  (par exemple : mise à jour des instances), stratégique (ajout ou retrait de
  fonctionnalité), ou en rapport avec des espaces ou des utilisateurs spécifiques
  (suppression d’espace, suppression de compte, etc), **sans obligation préalable
  d’avertissement des utilisateurs ou administrateurs d’espace**.

  Évidemment, Framasoft essaiera, si cela lui est possible, d’échanger en amont
  avec les personnes concernées (contacts directs, newsletters, etc).
  Cependant cette clause vise à affirmer que ces discussions préalables sont
  une possibilité, mais en aucun cas une obligation.

  ------------------------------------------------------------------------------

  ### PARTIE II - CONDITIONS SPÉCIFIQUES D’HÉBERGEMENT DES DONNÉES A CARACTÈRE PERSONNEL DU SERVICE @:txt.space DANS LE CADRE DU RÈGLEMENT GÉNÉRAL DE PROTECTION DES DONNÉES ENTRANT EN VIGUEUR LE 25 MAI 2018

  #### Article 1. Contexte et Objet

  Le Bénéficiaire ou Client, Responsable de Traitement, à souscrit à un ou plusieurs services auprès de Framasoft, dans le cadre des Conditions Générales Framasoft ou dans le cadre d’un contrat particulier.

  Le Bénéficiaire ou Client héberge des données à caractère personnel sur les serveurs de Framasoft ce qui donne à Framasoft le statut, conformément à la doctrine de la CNIL, de sous-traitant.

  Framasoft propose, en tant que sous-traitant, l’accès au Bénéficiaire ou Client à un service en ligne nommé @:txt.space, permettant au Bénéficiaire ou Client de proposer aux personnes choisies par ce dernier d’héberger elles-mêmes des données informatique de tous types.

  Les présentes clauses ont pour objet de définir les conditions dans lesquelles le sous-traitant (Framasoft) s’engage à effectuer pour le compte du Responsable de Traitement (les administrateur d’espaces @:txt.space) les opérations de traitement de données à caractère personnel définies ci-après.

  Dans le cadre de leurs relations contractuelles, les parties s’engagent à respecter la réglementation en vigueur applicable au traitement de données à caractère personnel et, en particulier, le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 applicable à compter du 25 mai 2018 (ci-après, « le règlement européen sur la protection des données »).

  Il est rappelé que dans le cadre de sa relation avec le Responsable de Traitement, Framasoft se limite à fournir de l’espace d’hébergement et n’intervient jamais directement sur les données à caractère personnel du Bénéficiaire ou Client, en ce sens qu’en aucun cas Framasoft ne manipulera les données à caractère personnel du Bénéficiaire ou Client, en dehors de leur stockage et de leur sauvegarde, ou d’interventions légales ou techniques indispensables au bon fonctionnement du service.

  En sa qualité d’hébergeur, Framasoft n’a pas d’obligation générale de surveillance du contenu qu’il héberge et, sauf souscription à un service particulier, ignore donc si ses Bénéficiaires ou Clients hébergent des données à caractère personnel sur ses services.

  #### Article 2. Description du traitement faisant l’objet de la sous-traitance

  Le sous-traitant est autorisé à héberger pour le compte du Responsable de Traitement, les données à caractère personnel que celui-ci lui confie dans le cadre du service @:txt.space, ainsi que leur sauvegarde.

  La nature des opérations réalisées sur les données correspond à l’hébergement et la sauvegarde des données, pour les services numériques suivants, ci-après dénommés « le service @:txt.space » :

  * un espace de discussion entre utilisateurs du service @:txt.space ;
  * une lettre d’information à destination des utilisateurs du service @:txt.space ;
  * la mise à disposition « d’espaces @:txt.space », une plateforme logicielle pouvant manipuler des données.

  Concernant la collecte et le traitement des données personnelles permettant d’opérer le service Framasoft, les Clients ou Bénéficiaires du service @:txt.space peuvent se référer aux informations indiquées sur la page [Vie privée et RGPD](https://www.frama.space/abc/fr/rgpd/).

  Concernant les données collectées ou traitées au sein des espaces @:txt.space :

  * La ou les finalités du traitement est ignorée par le Sous-Traitant, conformément à l’article 6-1-2 de la loi n°2004- 575 du 21 juin 2004.
  * Les données à caractère personnel traitées sont ignorées par le Sous-Traitant, conformément à l’article 6-1-2 de la loi n°2004-575 du 21 juin 2004.
  * Les catégories de personnes concernées sont ignorées par le Sous-Traitant, conformément à l’article 6-1-2 de la loi n°2004-575 du 21 juin 2004.

  #### Article 3. Durée du contrat

  Les présentes conditions générales d’hébergement de données à caractère personnel entrent en vigueur à compter du 18 octobre 2022.

  Le contrat prend fin deux mois après la désinscription du service @:txt.space par le Bénéficiaire ou Client, ou à l’arrêt du service @:(txt.space).

  #### Article 4. Obligations du sous-traitant vis-à-vis du Responsable de Traitement

  Le sous-traitant (Framasoft) s’engage à :

  1. Traiter les données uniquement pour les seules finalités qui font l’objet de la sous-traitance, à savoir héberger les données et permettre les interventions légales et techniques indispensables au bon fonctionnement du service @:(txt.space). Il est entendu que le sous-traitant n’effectue aucune action sur les données à caractère personnel du responsable du traitement.

  2. traiter les données conformément aux services souscrits par le Bénéficiaire ou Client. Si le sous-traitant considère qu’une instruction constitue une violation du règlement européen sur la protection des données ou de toute autre disposition du droit de l’Union ou du droit des États membres relative à la protection des données, il en informe au plus vite le Responsable de Traitement. En outre, si le sous-traitant est tenu de procéder à un transfert de données vers un pays tiers ou à une organisation internationale, en vertu du droit de l’Union ou du droit de l’État membre auquel il est soumis, il doit informer le responsable du traitement de cette obligation juridique avant le traitement, sauf si le droit concerné interdit une telle information pour des motifs importants d’intérêt public.

  3. Garantir la confidentialité des données à caractère personnel traitées dans le cadre du présent contrat (dans la mesure où le Responsable du Traitement ne rend pas son hébergement accessible à des tiers et veille à ce que les mesures de sécurité permettant la confidentialité soient prises, puisque le Bénéficiaire ou Client a un contrôle étendu sur les données à caractère personnel hébergées par Framasoft au travers du service @:txt.space).

  4. Veiller à ce que les personnes autorisées à traiter les données à caractère personnel en vertu du présent contrat :
     * S’engagent à respecter la confidentialité des dites données ;
     * Reçoivent les informations nécessaires en matière de protection des données à caractère personnel.

  5. Prendre en compte, s’agissant de ses outils, produits, applications ou services, les principes de protection des données dès la conception et de protection des données par défaut.

  #### Article 5. Sous-traitance

  Le sous-traitant peut faire appel à un autre sous-traitant (ci- après, « Le sous-traitant ultérieur ») pour mener des activités de traitement ou d’hébergement spécifiques. La liste des sous-traitants ultérieurs est indiquée sur la page [Vie privée et RGPD, section « Comment partageons-nous les données à caractère personnel ? »](https://www.frama.space/abc/fr/rgpd).

  Dans le cadre du service @:txt.space, il est rappelé que les données sont hébergées par la société européenne de droit allemand  [Hetzner](https://hetzner.com). Les engagements de ce sous-traitant concernant la gestion des données personnelles est [disponible en ligne](https://www.hetzner.com/legal/privacy-policy). Il est à noter que cet hébergeur n’est pas certifié HDS (Hébergeur de Données de Santé).

  Il informe préalablement le Responsable de Traitement de tout changement envisagé concernant l’ajout ou le remplacement d’autres sous-traitants. Cette information doit indiquer clairement les activités de traitement ou d’hébergement sous-traitées, l’identité et les coordonnées du sous-traitant et les dates du contrat de sous-traitance.

  Le sous-traitant ultérieur est tenu de respecter les obligations du présent contrat pour le compte et selon les instructions du Responsable de Traitement. Il appartient au sous-traitant initial (Framasoft) de s’assurer que le sous-traitant ultérieur (Hetzner) présente les mêmes garanties suffisantes quant à la mise en œuvre de mesures techniques et organisationnelles appropriées de sorte que le traitement réponde aux exigences du règlement européen sur la protection des données.

  Si le sous-traitant ultérieur ne remplit pas ses obligations en matière de protection des données, le sous-traitant initial demeure pleinement responsable devant le Responsable de Traitement de l’exécution par l’autre sous-traitant de ses obligations.

  #### Article 6. Droit d’information des personnes concernées

  Il appartient au Responsable de Traitement de fournir aux personnes concernées par les opérations de traitement, au moment de la collecte des données, les informations concernant leurs droit d’accès, de rectification, d’effacement et d’opposition, droit à la limitation du traitement, droit à la portabilité des données, droit de ne pas faire l’objet d’une décision individuelle automatisée (y compris le profilage). .

  #### Article 7. Exercice des droits des personnes

  Il appartient au Responsable de Traitement à s’acquitter de son obligation de donner suite aux demandes d’exercice des droits des personnes concernées : droit d’accès, de rectification, d’effacement et d’opposition, droit à la limitation du traitement, droit à la portabilité des données, droit de ne pas faire l’objet d’une décision individuelle automatisée (y compris le profilage).

  Si un Bénéficiaire ou Client du service @:txt.space n’obtient pas de réponse du Responsable de Traitement de (administrateur de l’espace @:txt.space) qui héberge des données à caractère personnel sous le délai courant d’un mois maximum (cf. [https://www.cnil.fr/fr/professionnels-comment-repondre-une-demande-de-droit-dacces](https://www.cnil.fr/fr/professionnels-comment-repondre-une-demande-de-droit-dacces) ), il peut alors saisir le sous-traitant (Framasoft) afin de faire valoir ses droits.

  Lorsque les personnes concernées exercent auprès du sous-traitant des demandes d’exercice de leurs droits, le sous-traitant doit s’assurer que le Responsable de Traitement a bien été sollicité en amont en le contactant par courrier électronique à l’adresse indiqué par le Bénéficiaire ou Client au moment de la souscription aux services.

  #### Article 8. Notification des violations de données à caractère personnel

  Le sous-traitant notifie au Responsable de Traitement toute violation de données à caractère personnel dans les meilleurs délais après en avoir pris connaissance et par email à l’adresse indiquée par le Bénéficiaire ou Client au moment de la souscription des services.

  Cette notification est accompagnée de toute documentation utile afin de permettre au Responsable de Traitement, si nécessaire, de notifier cette violation à l’autorité de contrôle compétente.

  La notification contient au moins :
  * la description de la nature de la violation de données à caractère personnel y compris, si possible, les catégories et le nombre approximatif de personnes concernées par la violation et les catégories et le nombre approximatif d’enregistrements de données à caractère personnel concernés ;
  * le nom et les coordonnées du délégué à la protection des données ou d’un autre point de contact auprès duquel des informations supplémentaires peuvent être obtenues ;
  * la description des conséquences probables de la violation de données à caractère personnel ;
  * la description des mesures éventuelles prises par le sous-traitant ;
  * la demande des action que le responsable du traitement propose de prendre pour remédier à la violation de données à caractère personnel, y compris, le cas échéant, les mesures pour en atténuer les éventuelles conséquences négatives.

  Dans la mesure où il n’est pas possible de fournir toutes ces informations en même temps, les informations peuvent être communiquées de manière échelonnée sans retard indu.

  Le Responsable du Traitement assume la communication auprès des personnes concernées des violations des données à caractère personnel. Il est rappelé que le sous-traitant n’a pas connaissance a priori des données à caractère personnel qu’il héberge et qu’il n’est donc pas susceptible de déterminer si une violation des données à caractère personnel est susceptible d’engendrer un risque élevé pour les droits et libertés d’une personne physique.

  #### Article 9. Aide du sous-traitant dans le cadre du respect par le Responsable de Traitement de ses obligations

  Le sous-traitant communiquera au Responsable de Traitement la documentation pertinente pour la réalisation d’analyses d’impact relative à la protection des données par ce dernier, s’agissant uniquement des aspects dont le sous-traitant à la charge.

  #### Article 10. Mesures de sécurité

  Au regard de « bonnes pratiques », le sous-traitant a mis en place des mesures qui lui semblent proportionnées aux risques identifiés, en agissant sur :

  * **les « éléments à protéger »** : la collecte de données par le sous-traitant est minimisée, le transport des communication réseau est chiffrée (SSL), les informations personnelles peuvent être pseudonymisées, Framasoft permet aux utilisateurs de faire valoir l’exercice de leurs droits.
  * **les « impacts potentiels »**: les données sont sauvegardées ou redondées, les activités sont tracées, les utilisateurs sont prévenus en cas de violation de données ou d’intrusion détectées.
  * **les « sources de risques »**: les accès aux serveurs sont contrôlés et réservés aux personnes habilités par Framasoft, les accès aux espaces de production utilisateurs sont contrôlés par les administrateurs d’espaces, Framasoft exerce une surveillance active contre les codes malveillants, Framasoft met régulièrement à jour les codes informatiques des logiciels utilisés dans leurs dernières versions.
  * **les « supports »** : les accès physiques au machines sont limités aux personnes habilitées. Afin de réduire les risques logiciels Framasoft n’utilise que des logiciels libres et opensource.

  Il est rappelé que dans le cadre de là prestation d’hébergement, le Responsable du Traitement décide lui-même de la politique de sécurité qu’il souhaite mettre en place. Les mesures de sécurité mise en place par le sous-traitant ne se substituent pas aux mesures de sécurité que doit prendre le Responsable de Traitement pour les traitements de données à caractère personnel afin de s’assurer de la conformité de ses traitements au RGPD.

  #### Article 11. Sort des données à l’issue de la relation

  L’ensemble des données (à caractère personnel ou non) disponibles sur l’espace @:txt.space du Bénéficaire ou Client sont supprimées :

  * dans les 7 jours ouvrés suivant la demande de suppression d’un compte utilisateur par ledit utilisateur, étant entendu que la demande peut se faire auprès du Responsable de Traitement, ou directement par l’utilisateur au sein du service @:txt.space ;
  * dans les 7 jours ouvrés suivant la demande de suppression de l’espace @:txt.space par le Bénéficiaire ou Client ayant souscrit le service, étant entendu que la demande se fait auprès du sous-traitant.

  Pour rappel, les sauvegardes sont conservées deux mois, et les données de journalisation 12 mois (glissants).

  #### Article 12. Délégué à la protection des données

  Les coordonnées du délégué à la protection des données (DPO) sont accessibles sur la page « [Vie privée et RGPD](https://www.frama.space/abc/fr/rgpd/) et le Bénéficiaire ou Client peut le contacter via cette page  (ou via dpo@framasoft.org ).

  #### Article 13. Documentation

  Le sous-traitant met à la disposition du Responsable de Traitement la documentation nécessaire pour démontrer le respect de toutes ses obligations, dans la limite du rôle du sous-traitant, à savoir l’hébergement des données du responsable du traitement.

  #### Article 14. Registre des catégories d’activités de traitement

  Le sous-traitant déclare **tenir par écrit un registre** de toutes les catégories d’activités de traitement effectuées pour le compte du responsable de traitement

  #### Article 15. Obligations du Responsable de Traitement vis-à-vis du sous-traitant

  **Le responsable de traitement s’engage à :**

    1. Veiller, au préalable et pendant toute la durée du traitement, au **respect des obligations prévues par le règlement européen sur la protection des données (RGPD)**. Que cela soit en termes de collecte (notamment en précisant le type et la finalité de traitement des données recueillies, lorsqu’elles rentrent dans le cadre de la définition de l’article 4 du RGPD), ou dans le cadre d’hébergement ou de diffusion de telles données.
    2. Superviser le Traitement, y compris réaliser les audits et les inspections auprès du Sous-traitant.
    3. Faire son affaire personnelle de la sauvegarde des données récoltées dans le cadre de l’utilisation des services @:txt.space, au-delà de la période visée aux articles 10 et 11 ci-avant.

  **Pour rappel, les obligations principales du Responsable de Traitement vis-à-vis des utilisateurs de l’espace @:txt.space qu’il administre sont les suivantes :**

  1. Principe de finalité : vos traitements doivent avoir un but précisément défini
  2. Principe de minimisation : seules les données nécessaires à la finalité doivent être traitées.
  3. Limitation de la durée de conservation des données.
  4. prendre en compte, s’agissant de vos outils numériques de traitement de données personnelles, les principes de **protection des** **Données personnelles** **dès la conception** et de **protection des** **Données personnelles** **par défaut**
  5. Obligation d’information des personnes : les utilisateurs de l’espace @:txt.space doivent être informés de la finalité, des données collectées, de leurs destinataires, de leur durée de conservation, du type de traitements réalisés, des  mesures de sécurité prises pour la protection des données, des droits  qu’elles détiennent sur ces données.
  6. La conclusion de contrat avec les sous-traitants en matière de traitement de données personnelles, prévoyant les garanties de sécurité  et de confidentialité imposées à ce dernier, qui ne pourra pas recruter  un autre sous-traitant sans votre accord préalable.
  7. Assurer la sécurité du traitement : prendre les mesures de  sécurité techniques et organisationnelles appropriées compte tenu de  l’état des connaissances, des coûts de mise en œuvre et de la nature, du traitement ainsi que des risques pour les droits et libertés des  personnes physiques.
  8. Garantir les droits des personnes : répondre dans un délai d’un mois aux personnes concernées qui souhaitent faire valoir leurs droits sur leurs données.
  9. Recueillir le consentement des utilisateurs de l’espace @:txt.space en cas de collecte de données à caractère personnel les concernant.
  10. Garantir les droits des utilisateurs de l’espace @:txt.space : droit à l’effacement, droit à la limitation du traitement, droit à la portabilité des données.
  11. Conserver la confidentialité des données personnelles des utilisateurs de l’espace @:txt.space et se limiter à leur seule diffusion aux destinataires des données personnelles portées à la connaissance des utilisateurs de l’espace @:(txt.space).
  12. Notifier les failles de sécurité dans les 72h au sous-traitant ainsi qu’à la personne concernée si elle est susceptible d’engendrer un risque élevé pour les droits et libertés d’une personne physique,
  13. Établir un registre des activités de traitement, sauf cas particulier,
  14. Désigner un délégué à la protection des données (DPO) si vous remplissez certaines conditions.
  15. Mener une étude d’impact si vous remplissez certaines conditions.

  #### Article 16. Portée des conditions générales d’échange de données

  Les présentes conditions spécifiques d’hébergement de données à caractère personnel dans le cadre du Règlement général de protection des données entrant en vigueur le 25 mai 2018 et les Conditions Générales Framasoft ou le contrat particulier conclu avec le Bénéficiaire ou Client forment un document contractuel unique.

  Toutes les stipulations des Conditions Générales Framasoft ou du contrat particulier auxquelles les présentes Conditions Spécifiques d’hébergement de données à caractère personnel ne déroge pas ou sont non contradictoires avec les termes des Conditions Spécifiques d’hébergement de données à caractère personnel demeurent pleinement applicables entre les parties. En cas de divergence entre les Conditions Générales Framasoft et les présents Spécifiques Générales d’hébergement de données à caractère personnel, les présentes Conditions Générales d’hébergement de données à caractère personnel prévaudront à compter de leur date d’entrée en vigueur.

  Si l’une des stipulations des Conditions Spécifiques d’hébergement de données à caractère personnel s’avérait nulle, au regard d’une règle de droit en vigueur ou d’une décision judiciaire devenue définitive, elle serait réputée non écrite, sans pour autant entraîner la nullité des présentes Conditions Spécifiques d’hébergement de données à caractère personnel ni altérer la validité de ses autres dispositions.

  ------------------------------------------------------------------------------

  ### PARTIE III - MISE À JOUR DES CONDITIONS SPÉCIFIQUES D’UTILISATION DU SERVICE @:txt.space

  Framasoft est susceptible de mettre à jour les présentes Conditions Spécifiques d’Utilisation du service @:txt.space à tout moment. Les modifications seront alors publiées sur le site. Si vous n’acceptez pas les modifications publiées, vous devrez alors cesser toute utilisation du service.

  En cas de modifications mineures, nous changerons la date de mise à jour, située en bas de cette page, en indiquant la date à laquelle les modifications ont été apportées (Nous vous conseillons de consulter régulièrement cette page afin de prendre connaissance des éventuelles modifications ou mises à jour apportées à notre politique).

  En cas de modifications majeure, nous changerons la date de mise à jour, située en bas de cette page, en indiquant la date à laquelle les modifications ont été apportées. Framasoft s’engage alors à informer les Responsables de Traitement dans un délai d’un mois.

  ------

  <p class="text-right">
    <sub>Date de publication : 05/10/2022 - Date de dernière mise à jour : 12/10/2022 </sub><br />
    <sub>[Historique](https://framagit.org/framasoft/@:txt.space/home/-/commits/master/src/translations/fr/csu.yml)</sub>
  </p>
